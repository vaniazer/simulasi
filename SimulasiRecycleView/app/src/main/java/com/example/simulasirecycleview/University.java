package com.example.simulasirecycleview;

public class University {
    private String sName, sDetail, sPhoto;
    private String sInfo;

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getsDetail() {
        return sDetail;
    }

    public void setsDetail(String sDetail) {
        this.sDetail = sDetail;
    }

    public String getsPhoto() {
        return sPhoto;
    }

    public void setsPhoto(String sPhoto) {
        this.sPhoto = sPhoto;
    }

    public void setsInfo(String s) {
        this.sInfo =  sInfo;
    }
}

