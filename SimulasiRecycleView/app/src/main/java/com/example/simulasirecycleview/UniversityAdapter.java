package com.example.simulasirecycleview;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;


public class UniversityAdapter extends RecyclerView.Adapter<UniversityAdapter.ViewHolder>{
    private ArrayList<University> univerSity;
    private Context context;



    public UniversityAdapter(ArrayList<University> univerSity,Context context) {
        this.univerSity = new ArrayList<>();
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_app, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final University university = univerSity.get(position);
        Glide.with(holder.itemView.getContext())
                .load(university.getsPhoto())
                .apply(new RequestOptions().override(55,55))
                .into(holder.ivPhoto);
        holder.tvName.setText(university.getsName());
        holder.tvDetail.setText(university.getsDetail());

        holder.cvCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("name", university.getsName());
                intent.putExtra("detail", university.getsDetail());
                intent.putExtra("photo", university.getsPhoto());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return univerSity.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvDetail;
        ImageView ivPhoto;
        CardView cvCardView;
        LinearLayout llLinearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tv_name);
            tvDetail = itemView.findViewById(R.id.tv_detail);
            ivPhoto = itemView.findViewById(R.id.iv_photo);
            cvCardView = itemView.findViewById(R.id.cv_cardview);
            llLinearLayout = itemView.findViewById(R.id.ll_linear);
        }

    }
}
