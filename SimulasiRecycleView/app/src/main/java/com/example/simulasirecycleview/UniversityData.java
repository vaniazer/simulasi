package com.example.simulasirecycleview;

import java.util.ArrayList;

public class UniversityData {
    private static String[] appNames = {
            "Massachusetts Institute of Technology",
            "Stanford University",
            "Harvard University",
            "California Institute of Technology",
            "University of Oxford",
            "ETH Zurich",
            "University of Cambridge",
            "Imperial College London",
            "University of Chicago",
            "UCL"
    };

    private static String[] appDetails = {
            "United States",
            "United States",
            "United States",
            "United States",
            "United Kingdom",
            "Switzerland",
            "United Kingdom",
            "United Kingdom",
            "United States",
            "United Kingdom",
    };

    private static String[] appImages = {
            "https://upload.wikimedia.org/wikipedia/id/thumb/4/44/MIT_Seal.svg/1200px-MIT_Seal.svg.png",
            "https://1000logos.net/wp-content/uploads/2018/02/Stanford-Logo.png",
            "https://i.pinimg.com/originals/72/6a/30/726a303f6704bc2f24401db85e65f984.png",
            "https://upload.wikimedia.org/wikipedia/en/thumb/a/a4/Seal_of_the_California_Institute_of_Technology.svg/1200px-Seal_of_the_California_Institute_of_Technology.svg.png",
            "https://upload.wikimedia.org/wikipedia/id/8/8a/Oxford_University.png",
            "https://ulpecproject.eu/wp-content/uploads/2018/10/ETH-Logo.png",
            "https://upload.wikimedia.org/wikipedia/commons/a/a3/Cambridge_University_Crest_-_flat.png",
            "https://upload.wikimedia.org/wikipedia/commons/c/c3/Imperial_College_London.svg",
            "https://upload.wikimedia.org/wikipedia/en/thumb/7/79/University_of_Chicago_shield.svg/1200px-University_of_Chicago_shield.svg.png",
            "https://iafor.org/wp-content/uploads/2017/06/UCL-Logo-1.jpg"
    };

    private static String[] appInfo = {
            "“Mind and Hand” is the thought-provoking motto of the Massachusetts Institute of Technology, known also as MIT. This motto enigmatically encapsulates this famous institution’s mission to advance knowledge in science, technology and areas of scholarship that can help to make the world a better place. At its founding in 1861, MIT was initially a small community of",
            "Located 35 miles south of San Francisco and 20 miles north of San Jose, Stanford University is in the heart of Northern California’s dynamic Silicon Valley, home to Yahoo, Google, Hewlett-Packard, and many other cutting-edge tech companies that were founded by and continue to be led by Stanford alumni and faculty. Nicknamed the “billionaire factory”, it is said that if Stanford graduates formed their own country it would boast one of the world’s largest ten economies. ",
            "Located in Cambridge, Massachusetts, three miles north-west of Boston, Harvard’s 209-acre campus houses 10 degree-granting schools in addition to the Radcliffe Institute for Advanced Study, two theaters, and five museums. It is also home to the largest academic library system in the world, with 18 million volumes, 180,000 serial titles, an estimated 400",
            "Caltech has a high research output as well as many high-quality facilities such as the Jet Propulsion Laboratory (owned by NASA), the Caltech Seismological Laboratory, and the International Observatory Network.  It’s among a small group of institutes of technology in the United States primarily devoted to teaching technical arts and applied sciences, and its fiercely competitive admissions process ensures only a small number of the most gifted students are admitted.",
            "The University of Oxford is the oldest university in the English-speaking world, and is actually so ancient that its founding date is unknown – though it is thought that teaching took place there as early as the 11th century. ",
            "ETH Zurich is one of the world's leading universities in science and technology and is known for its cutting-edge research and innovation. It was established in 1855 as the Swiss Federal Polytechnic School, and a century and a half later the university can count 21 Nobel laureates, 2 Fields Medalists, 2 Pritzker Prize winners, and 1 Turing Award winner as alumni, including the great Albert Einstein himself. ",
            "Located in the center of the ancient city of Cambridge, 50 miles north of London, the University of Cambridge is a collegiate public research institution that serves more than 18,000 students from all corners of the globe. ",
            "Imperial is based in South Kensington in London, in an area known as ‘Albertopolis’, Prince Albert and Sir Henry Cole’s 19th century vision for an area where science and the arts would come together. As a result, Imperial’s neighbors include a number of world leading cultural organizations including the Science, Natural History and Victoria and Albert museums; the Royal Colleges of Art and Music; the English National Ballet; and the Royal Albert",
            "Established in 1856, the University of Chicago is a private research university based in the urban center of Chicago, the third most populous city in the United States. Outside of the Ivy League, Chicago is one of America’s top universities, and holds top-ten positions in various national and international rankings. ",
            "UCL is 8th in the world, 4th in Europe, 1st in London in the QS World University Rankings 2020. There has been 29 Nobel laureates among its former staff and students; a winner in every decade since the Prize started.At the very heart of UCL’s mission is our research. We were rated the top university in the UK for research strength in the last Research Excellence Framework (REF 2014). We offer over 675 postgraduate taught, research and initial teacher education programmes across a wide range of disciplines, and encourage our community to work across traditional subject boundaries. UCL's research-based"
    };

    static ArrayList<University> getListData() {
        ArrayList<University> list = new ArrayList<>();

        for (int position = 0; position < appNames.length; position++) {
            University university = new University();
            university.setsName(appNames[position]);
            university.setsDetail(appDetails[position]);
            university.setsPhoto(appImages[position]);
            university.setsInfo(appInfo[position]);
            list.add(university);
        }
        return list;
    }
}

