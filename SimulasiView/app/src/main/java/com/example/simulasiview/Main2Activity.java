package com.example.simulasiview;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Main2Activity extends AppCompatActivity {
    String n,e,c,m;
    TextView vd_Nama, vd_Email, vd_City, vd_Major;
    Button Implicitintent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        vd_Nama = findViewById(R.id.vd_nama);
        vd_Email = findViewById(R.id.vd_email);
        vd_City = findViewById(R.id.vd_city);
        vd_Major = findViewById(R.id.vd_major);
        Implicitintent = findViewById(R.id.implicitintent);

        Implicitintent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Akun();
            }


        });

        Bundle bundle=getIntent().getExtras();

        n = bundle.getString("nama");
        e = bundle.getString("email");
        c = bundle.getString("city");
        m = bundle.getString("major");

        vd_Nama.setText(n);
        vd_Email.setText(e);
        vd_City.setText(c);
        vd_Major.setText(m);



    }

    void Akun(){

        Intent implicit = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/itcupnyk/"));
        startActivity(implicit);

    }
}
