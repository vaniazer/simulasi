package com.example.simulasiview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button btn_Cancel, btn_Sign;
    EditText et_Nama, et_Email, et_Pass, et_City, et_Major, et_Motiv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_Cancel = findViewById(R.id.btn_cancel);
        btn_Sign = findViewById(R.id.btn_sign);
        et_Nama = findViewById(R.id.et_nama);
        et_Email = findViewById(R.id.et_email);
        et_Pass = findViewById(R.id.et_pass);
        et_City = findViewById(R.id.et_city);
        et_Major = findViewById(R.id.et_major);
        et_Motiv = findViewById(R.id.et_motiv);
        
        btn_Sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkData();
            }


        });

        btn_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset();
            }
        });
       
    }

        boolean isEmail(EditText text) {
            CharSequence email = text.getText().toString();
            return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
        }

        boolean isEmpty(EditText text) {
            CharSequence str = text.getText().toString();
            return TextUtils.isEmpty(str);
        }

        void checkData() {
            if (isEmpty(et_Nama)) {
                Toast t = Toast.makeText(this, "You must enter fullname to register!", Toast.LENGTH_SHORT);
                t.show();
            }

            if (isEmpty(et_Pass)) {
                et_Pass.setError("Password is required!");
            }

            if (isEmpty(et_City)) {
                et_City.setError("City is required!");
            }

            if (isEmpty(et_Major)) {
                et_Major.setError("Major is required!");
            }

            if (isEmpty(et_Motiv)) {
                et_Motiv.setError("Major is required!");
            }

            if (isEmail(et_Email) == false) {
                et_Email.setError("Enter valid email!");
            }

            else{
                Intent intent = new Intent(MainActivity.this,Main2Activity.class);
                intent.putExtra("nama", et_Nama.getText().toString());
                intent.putExtra("email", et_Email.getText().toString());
                intent.putExtra("city", et_City.getText().toString());
                intent.putExtra("major", et_Major.getText().toString());
                startActivity(intent);
                Toast sign = Toast.makeText(this, "Sign Up Succesfully!", Toast.LENGTH_SHORT);
                sign.show();
            }

        }

        void reset(){
            et_Nama.setText("");
            et_Email.setText("");
            et_Pass.setText("");
            et_City.setText("");
            et_Major.setText("");
            et_Motiv.setText("");

            Toast text = Toast.makeText(this, "Reset Succesfully!", Toast.LENGTH_SHORT);
            text.show();
        }
    }
